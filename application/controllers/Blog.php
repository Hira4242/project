<?php
class Blog extends CI_Controller {

        public function index()
        {
                echo 'Hello World!';
        }

        public function blogview() // load a  view with name 'blogview'
        {
        	$this->load->view('blogview');
        }

        public function blogview_dynamic() // send dynamic data to view named "myblogview"
        {
                $data['title'] = "My Real Title";
                $data['heading'] = "My Real Heading";

                $this->load->view('myblogview', $data);
        }
        public function multiple_views()  // loading more than 1 views also sending dynamic data
        {
                $data['title'] = "My Real Title";
                $data['heading'] = "My Real Heading";
                $this->load->view('myblogview', $data);
                $this->load->view('blogview');
        }
}